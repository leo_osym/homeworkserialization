﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace HomeworkSerialization
{
    public class Program
    {
        public class Auditory
        {
            [XmlAttribute]
            public string Name { get; set; }
            //public DateTime time { get; set; }
        }
        public class Lecturer
        {
            [XmlAttribute]
            public string FirstName { get; set; }
            [XmlAttribute]
            public string LastName { get; set; }
        }
        public class Subject
        {
            [XmlAttribute]
            public string SubjectName { get; set; }
            public List<Lecturer> Lecturers { get; set; } = new List<Lecturer>();
            public List<Auditory> Auditories { get; } = new List<Auditory>();
        }
        public class Mark
        {
            public Subject Subject { get; set; }
            [XmlAttribute]
            public int Value { get; set; }
        }
        public class Student
        {
            [XmlAttribute]
            public string FirstName { get; set; }
            [XmlAttribute]
            public string LastName { get; set; }
            public List<Mark> Marks { get; } = new List<Mark>();
        }
        public class Group
        {
            [XmlAttribute]
            public string Name { get; set; }
            public List<Student> Students { get; } = new List<Student>();
        }
        public class Specialization
        {
            [XmlAttribute]
            public string Name { get; set; }
            public List<Group> Groups { get; } = new List<Group>();
        }
        public class University
        {
            [XmlAttribute]
            public string Name { get; set; }
            public List<Specialization> Specializations { get; } = new List<Specialization>();
        }
        static void Main(string[] args)
        {
            using (Stream stream = File.OpenWrite("uni.xml"))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(University));
                University uni = InitUniversity();

                serializer.Serialize(stream, uni);
            }
        }

        public static University InitUniversity()
        {
            Auditory au201 = new Auditory { Name = "201" };
            Auditory au202 = new Auditory { Name = "202" };
            Auditory au203 = new Auditory { Name = "203" };
            Auditory au204 = new Auditory { Name = "204" };
            Auditory au205 = new Auditory { Name = "205" };

            Lecturer lecturer1 = new Lecturer { LastName = "Paradise", FirstName = "Sal" };
            Lecturer lecturer2 = new Lecturer { LastName = "Moriarty", FirstName = "Dean" };
            Lecturer lecturer3 = new Lecturer { LastName = "King", FirstName = "Chad" };
            Lecturer lecturer4 = new Lecturer { LastName = "Frei", FirstName = "Max" };
            Lecturer lecturer5 = new Lecturer { LastName = "Rose", FirstName = "Brian" };

            Subject subject1 = new Subject { SubjectName = "Rocket Math" };
            subject1.Lecturers.Add(lecturer1);
            subject1.Lecturers.Add(lecturer2);
            subject1.Auditories.Add(au201);

            Subject subject2 = new Subject { SubjectName = "Physics" };
            subject2.Lecturers.Add(lecturer3);
            subject2.Auditories.Add(au202);

            Subject subject3 = new Subject { SubjectName = "Chemistry" };
            subject3.Lecturers.Add(lecturer4);
            subject3.Auditories.Add(au201);
            subject3.Auditories.Add(au203);

            Subject subject4 = new Subject { SubjectName = "Termodinamics" };
            subject4.Lecturers.Add(lecturer5);
            subject4.Auditories.Add(au204);

            Student student1 = new Student { FirstName = "Leo", LastName = "Osym" };
            student1.Marks.Add(new Mark { Subject = subject1, Value = 83 });
            student1.Marks.Add(new Mark { Subject = subject2, Value = 78 });
            student1.Marks.Add(new Mark { Subject = subject3, Value = 95 });

            Student student2 = new Student { FirstName = "Axel", LastName = "Fox" };
            student2.Marks.Add(new Mark { Subject = subject1, Value = 69 });
            student2.Marks.Add(new Mark { Subject = subject2, Value = 70 });
            student2.Marks.Add(new Mark { Subject = subject3, Value = 78 });

            Student student3 = new Student { FirstName = "Tom", LastName = "Spolding" };
            student3.Marks.Add(new Mark { Subject = subject1, Value = 95 });
            student3.Marks.Add(new Mark { Subject = subject3, Value = 83 });
            student3.Marks.Add(new Mark { Subject = subject4, Value = 76 });

            Student student4 = new Student { FirstName = "Douglas", LastName = "Spolding" };
            student4.Marks.Add(new Mark { Subject = subject1, Value = 90 });
            student4.Marks.Add(new Mark { Subject = subject3, Value = 87 });
            student4.Marks.Add(new Mark { Subject = subject4, Value = 89 });

            Student student5 = new Student { FirstName = "Maxine", LastName = "Caulfield" };
            student5.Marks.Add(new Mark { Subject = subject2, Value = 56 });
            student5.Marks.Add(new Mark { Subject = subject3, Value = 65 });
            student5.Marks.Add(new Mark { Subject = subject4, Value = 73 });

            Student student6 = new Student { FirstName = "Victoria", LastName = "Chase" };
            student6.Marks.Add(new Mark { Subject = subject2, Value = 99 });
            student6.Marks.Add(new Mark { Subject = subject3, Value = 99 });
            student6.Marks.Add(new Mark { Subject = subject4, Value = 99 });

            Group group1 = new Group { Name = "GR2015-1" };
            group1.Students.Add(student1);
            group1.Students.Add(student2);

            Group group2 = new Group { Name = "GR2015-2" };
            group2.Students.Add(student3);
            group2.Students.Add(student4);

            Group group3 = new Group { Name = "GR2015-3" };
            group3.Students.Add(student5);
            group3.Students.Add(student6);

            Specialization sp1 = new Specialization { Name = "SP-1" };
            sp1.Groups.Add(group1);
            sp1.Groups.Add(group2);

            Specialization sp2 = new Specialization { Name = "SP-2" };
            sp2.Groups.Add(group3);

            University uni = new University { Name = "Some National University" };
            uni.Specializations.Add(sp1);
            uni.Specializations.Add(sp2);
            return uni;
        }
    }
}
